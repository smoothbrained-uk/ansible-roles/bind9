# Role for deploying named on FreeBSD

```yaml
named_acls:
  home:
    - "192.168.0.0/24"

named_interfaces:
  - "127.0.0.1"
  - "192.168.0.254"

named_allow_recursion:
  - home

named_forwarders:
  - "8.8.8.8" 
  - "8.8.4.4"

named_zones:
  - name: "example.com"
    source_file: "db.example.com"
  - name: "0.168.192.in-addr.arpa"
    source_file: "db.0.168.192.in-addr.arpa"
```

## Variables
- `named_zone_dir`: The path to the directory where zone files are copied to and also where named looks for zone files. Defaults to `/var/db/named`.
- `named_cache_dir`: The path to the named cache directory. Defaults to `/var/cache/named`.
- `named_pid_file`: The path of the pid file that named creates. Defaults to `/var/run/named.pid`.
- `named_hostname`: The hostname of this nameserver in named.conf. Defaults to `{{ inventory_hostname_short }}`
- `named_forwarders`: [Optional] [List] A list of forwarders for this nameserver.
- `named_zones`: [List] The zones for which this nameserver is an authority. Each zone in the list is a dictionary containing the following fields:
  - `name`: The name of the zone
  - `class`: The default record class. Defaults to `IN`.
  - `type`: Zone type. Defaults to `master`.
  - `source_file`: [Optional] The path to the zone file on the ansible controller to copy to the zone directory on the nameserver.
  - `file`: [Optional] What to call the zone file when it is copied into the zone directory
- `named_dnssec_validation`: [Bool] Whether or not DNSSEC validation is enabled. Defaults to `true`.
- `named_allow_recursion`: [Optional] [List] A list of ACLs that are allowed to perform recursive lookups.
- `named_acls`: [Optional] Each sub-dictionary in `named_acls` is an ACL _list_ to define in named.conf on the target. See the example above.
- `no_install`: Determines whether and installation of the BIND9 utilities is attempted. Defaults to `true`. Useful if DNS is currently not working on the nameserver and querying upstream packages is not possible.
- `named_flags` [Optional] Startup flags for named
